Source: librdf-helper-perl
Section: perl
Priority: optional
Build-Depends: cdbs,
 devscripts,
 perl,
 debhelper,
 dh-buildinfo,
 libclass-load-perl (>= 0.20),
 libossp-uuid-perl,
 libmoose-perl (>= 1.09),
 libmoosex-aliases-perl,
 librdf-query-perl,
 liburi-perl,
 perl (>= 5.10) | libtest-simple-perl (>= 0.88),
 librdf-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>,
 Dominic Hargreaves <dom@earth.li>
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/librdf-helper-perl
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/librdf-helper-perl.git
Homepage: http://search.cpan.org/dist/RDF-Helper/

Package: librdf-helper-perl
Architecture: all
Depends: ${perl:Depends}, ${misc:Depends}, ${cdbs:Depends}
Recommends: ${cdbs:Recommends}
Description: consistent, high-level API for working with RDF with Perl
 Resource Description Framework (RDF) is a standard model for data
 interchange on the Web.
 .
 RDF::Helper intends to simplify, normalize and extend Perl's existing
 facilities for interacting with RDF data.
 .
 RDF::Helper's goal is to offer a syntactic sugar which will enable
 developers to work more efficiently. To achieve this, it implements
 methods to work with RDF in a way that would be familiar to Perl
 programmers who are less experienced with RDF.
